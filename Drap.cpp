/*
 * DrapMap_main.cpp
 *
 * Program to do Map Routing using Dijkstra's Algorithm and
 * priority_queue in STL
 * CMPE 130
 *  Nisun Alade, Smruthi Danda, Jisoo Kim, Minjoung Kim
 */
#include "Drap.h"

int main() {
    char ch1, y1; // yes
    int startP, endP;
    string c1, c2;

    // Construct the adjacency list that represents our graph.
    vector< vector<pair<int, int> > > adjList = FormAdjList();

	cout << "Welcome to DrapMap!\n\n";
	do {/*
		cout << "Please select from the followings.\n"
						"1. Select the starting point.\n2. Select the destination point.\n";
		cin >> ch1;
		while(ch1 != '1' && ch1 != '2'){
			cout << "Wrong selection! Please enter 1 or 2.\n";
			cin >> ch1;
		}
		switch(ch1) {
			case '1': // Starting Point
				cin.ignore();
				startP = getStart();
				endP = getDestination();
				break;
			case '2': // Destinaton Point
				cin.ignore();
				endP = getDestination();
				startP = getStart();
				break;
		}*/
		startP = getStart();
	    vector< pair<int, int> > dist = DijkstraSP(adjList, startP);
	    endP = getDestination();
	    // Print the list.
	    PrintShortestPath(dist, startP, endP);
	    routing(dist, startP, endP);

		cout << "\n\nWould you like to try another trip? Enter y for yes.\n";
		cin >> y1;
		cin.ignore();
	} while(y1 == 'y' || y1 == 'Y');

    cout << "Thank you for using DrapMap! Goodbye!\n";
    return 0;

}

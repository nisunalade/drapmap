/*
 * DrapGraph.h
 *
 * Program to do Map Routing using Dijkstra's Algorithm and
 * priority_queue in STL
 * CMPE 130
 *  Nisun Alade, Smruthi Danda, Jisoo Kim, Minjoung Kim
 */
#ifndef DRAPGRAPH_H_
#define DRAPGRAPH_H_

#include <vector>
#include <queue>
#include <iostream>
using namespace std;

vector <vector< pair<int, int> > > FormAdjList() {
	int V = 7; // num of vertices
	vector <vector <pair <int, int> > > adjList;
	for(int i = 0; i < V; i++) {
		vector <pair <int, int> > row;
		adjList.push_back(row);
	}
	adjList[0].push_back(make_pair(1, 2));
	adjList[0].push_back(make_pair(2, 3));

	adjList[1].push_back(make_pair(0, 2));
	adjList[1].push_back(make_pair(5, 1));

	adjList[2].push_back(make_pair(0, 3));
	adjList[2].push_back(make_pair(5, 2));

	adjList[3].push_back(make_pair(1, 4));
	adjList[3].push_back(make_pair(4, 1));
	adjList[3].push_back(make_pair(6, 2));

	adjList[4].push_back(make_pair(3, 1));
	adjList[4].push_back(make_pair(5, 2));
	adjList[4].push_back(make_pair(6, 1));

	adjList[5].push_back(make_pair(1, 1));
	adjList[5].push_back(make_pair(2, 2));
	adjList[5].push_back(make_pair(4, 2));
	adjList[5].push_back(make_pair(6, 2));

	adjList[6].push_back(make_pair(3, 2));
	adjList[6].push_back(make_pair(4, 1));
	adjList[6].push_back(make_pair(5, 2));

    // Our graph is now represented as an adjacency list. Return it.
    return adjList;
    }

// Given an Adjacency List, find all shortest paths from "start" to all other vertices.
vector< pair<int, int> > DijkstraSP(vector< vector<pair<int, int> > > &adjList, int &start)
    {
    vector<pair<int, int> > dist; // First int is dist, second is the previous node.

    // Initialize all source->vertex as infinite.
    int n = adjList.size();
    for(int i = 0; i < n; i++)
        {
        dist.push_back(make_pair(1000000007, i)); // Define "infinity" as necessary by constraints.
        }

    // Create a PQ.
    priority_queue<pair<int, int>, vector< pair<int, int> >, greater<pair<int, int> > > pq;

    // Add source to pq, where distance is 0.
    pq.push(make_pair(start, 0));
    dist[start] = make_pair(0, start);;

    // While pq isn't empty...
    while(pq.empty() == false)
        {
        // Get min distance vertex from pq. (Call it u.)
        int u = pq.top().first;
        pq.pop();

        // Visit all of u's friends. For each one (called v)....
        for(int i = 0; i < adjList[u].size(); i++)
            {
            int v = adjList[u][i].first;
            int weight = adjList[u][i].second;

            // If the distance to v is shorter by going through u...
            if(dist[v].first > dist[u].first + weight)
                {
                // Update the distance of v.
                dist[v].first = dist[u].first + weight;
                // Update the previous node of v.
                dist[v].second = u;
                // Insert v into the pq.
                pq.push(make_pair(v, dist[v].first));
                }
            }
        }

    return dist;
    }

int stringToInt(string point) {
	int p1 = -1;

    if (point == "San Jose")
        p1 = 0;
    else if (point == "Union City")
        p1 = 1;
    else if (point == "Santa Clara")
        p1 = 2;
    else if (point == "Oakland")
        p1 = 3;
    else if (point == "Hayward")
        p1 = 4;
    else if (point == "Fremont")
        p1 = 5;
    else if (point == "San Ramon")
        p1 = 6;

	return p1;
}

string intToString(int point) {
	string p1 = "";

    if (point == 0)
        p1 = "San Jose";
    else if (point == 1)
        p1 = "Union City";
    else if (point == 2)
        p1 = "Santa Clara";
    else if (point == 3)
        p1 = "Oakland";
    else if (point == 4)
        p1 = "Hayward";
    else if (point == 5)
        p1 = "Fremont";
    else if (point == 6)
        p1 = "San Ramon";

	return p1;
}

int getStart() {
	string point;
	int startP = -1;
	cout << "Please select the starting point.\n"
			"\t1. San Jose 2. Union City 3. Santa Clara 4. Oakland\n"
			"\t5. Hayward 6. Fremont 7. San Ramon\n";
	getline(cin, point);
	while(point != "San Jose" && point != "Union City" && point != "Santa Clara" && point != "Oakland" &&
			point != "Hayward" && point != "Fremont" && point != "San Ramon"){
        cout << "Invalid selection! Please re-enter valid point.\n";
        getline(cin, point);
	}
	/*
	for(int i = 0; i < 9; i++) {
		cout << i+1 << ": " << list[i];
	}
	cout << endl;
	getline(cin, point);
	while(point != list[0] && point != list[1] && point != list[2] && point != list[3] && point != list[4] &&
			point != list[5] && point != list[6] && point != list[7] && point != list[8]) {
        cout << "Invalid selection! Please re-enter valid destination.\n";
        getline(cin, point);
	}*/
	cout << "\nThe entered starting point is '" << point << "'.\n";

    startP = stringToInt(point);

	return startP;
}

int getCurrent() {
	string point;
	int currentP = -1;
	cout << "Please select the current point.\n"
			"\t1. San Jose 2. Union City 3. Santa Clara 4. Oakland\n"
			"\t5. Hayward 6. Fremont 7. San Ramon\n";
	cin.ignore();
	getline(cin, point);
	while(point != "San Jose" && point != "Union City" && point != "Santa Clara" && point != "Oakland" &&
			point != "Hayward" && point != "Fremont" && point != "San Ramon"){
        cout << "Invalid selection! Please re-enter valid point.\n";
        getline(cin, point);
	}
	cout << "\nThe entered current point is '" << point << "'.\n";

    currentP = stringToInt(point);

	return currentP;
}

int getDestination() {
	string point;
	int endP = -1;
	cout << "\nPlease select the destinaton point.\n"
			"\t1. San Jose 2. Union City 3. Santa Clara 4. Oakland\n"
			"\t5. Hayward 6. Fremont 7. San Ramon\n";
	cin.ignore();
	getline(cin, point);
	while(point != "San Jose" && point != "Union City" && point != "Santa Clara" && point != "Oakland"
			&& point != "Hayward" && point != "Fremont" && point != "San Ramon") {
        cout << "Invalid selection! Please re-enter valid destination.\n";
        getline(cin, point);
	}
	/*
	while(startP == point) {
		cout << "Error! The destination point cannot be the same as the starting point.\n"
				"Please select another one.\n";
		cin >> point;
	}*/
	cout << "The entered destination point is '" << point << "'.\n";

    endP = stringToInt(point);

	return endP;
}

void printPath(vector< pair<int, int> > &dist, int &start, int &end) {
	int current = end;
		cout << "The path is: " << intToString(current);
		while(current != start) {
			current = dist[current].second;
			cout << " <- " << intToString(current);
		}
}

void PrintShortestPath(vector< pair<int, int> > &dist, int &start, int &end) {
    cout << "\nPrinting the shortest path from '" << intToString(start) << "' to '" << intToString(end) << "'...\n";
	cout << "\tThe distance between '" << intToString(start) << "' and '" << intToString(end) << "' is: " << dist[end].first << " mile(s).\n\t";
	printPath(dist, start, end);
}

bool routeUpdate(vector< pair<int, int> > &dist, int &start, int &end) {
	int c1;
	int current = end;
	if(start == end) {
		cout << "You have arrived!\n";
		return 0;
	}
	cout << "\n\t1. Would you like to stay with the current path?\n\t(";
	printPath(dist, start, end);
	cout << ")\n\t2. Would you like to change your current location?\n"
			"\t3. Would you like to change your destination?\n";
	cin >> c1;
	while(c1 != 1 && c1 != 2 && c1 != 3) {
		cout << "Incorrect selection! Please enter 1: stay or 2: update.\n";
		cin >> c1;
	}

	if(c1 == 1) {
		cout << "here?" << endl;
		int position[] = {0, 0, 0, 0, 0, 0, 0};
		for(int i = 0; i < 7; i++) {
			cout << "Entered";
			while(current != start) {
				position[i] = dist[current].second;
			}
		}
		cout << "current : " << position[0] << endl;

		//routeUpdate(dist, current, end);
	} else if(c1 == 2) {
		int currentP = -1;
		currentP = getCurrent();
		if(currentP == end) {
			cout << "You have arrived!\n";
			return 0;
		}
		cout << "\nThe current point is changed from '" << intToString(start) << "' to '" << intToString(currentP) << "'.\n";
	    vector< vector<pair<int, int> > > adjList = FormAdjList();
	    vector< pair<int, int> > dist2 = DijkstraSP(adjList, currentP);
		PrintShortestPath(dist2, currentP, end);
		cout << endl;
		return routeUpdate(dist2, currentP, end);
	} else {
		int destP = -1;
		destP = getDestination();
		if(destP == start) {
			cout << "You have arrived!\n";
			return 0;
		}
		cout << "\nThe destination point is changed from '" << intToString(end) << "' to '" << intToString(destP) << "'.\n";
	    vector< vector<pair<int, int> > > adjList = FormAdjList();
		PrintShortestPath(dist, start, destP);
		cout << endl;
		return routeUpdate(dist, start, destP);
	}
}

void routing(vector< pair<int, int> > &dist, int &start, int &end) {
	cout << "\n\nStarting the routes...\nStarting from " << intToString(start) << "...\n";
	routeUpdate(dist, start, end);
}

#endif /* DRAPGRAPH_H_ */
